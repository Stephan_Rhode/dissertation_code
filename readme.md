# Overview
This repository contains the supplementary material to run the numerical experiments of the dissertation:
*Robust and regularized algorithms for vehicle tractive force prediction and mass estimation*

# Requirements
All functions are written in Matlab script language. There is no need to install specific Matlab libraries. The software was tested with Matlab 2013b and 2014b.

# Run the code
To run all experiments at glance call

	runAllExperiments()
	
You will find in this file also the syntax to run individual experiments. For instance

	% Experiment 3.3
	linearMISOoe('RLS');
	
Best regards, Stephan