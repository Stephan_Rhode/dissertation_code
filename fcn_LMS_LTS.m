%==========================================================================
% Solves the linear equation AX=B using robust LMS or LTS
% for MISO systems
% Inputs:
% A-system input size (m,n)
% B-system output size (m,d) with d:=1 for MISO systems
% prob-see fcn_subsamples
% prob_outliers-see fcn_subsamples
% type-'LMS' or 'LTS'
% Outputs:
% X-the system parameters size (n,d)
% P-parameter variance size (n,n)
% e_stand-standardized residuals size (m,1)
% w-weighting vector, 0 is outlier, 1 is good data size (m,1)
% stdev-robust estimated noise stdev
% Rquad-robust correlation coefficient
%source: [Rousseeuw1987]

function [X, P, e_stand, w, stdev, Rquad]  = fcn_LMS_LTS(A, B, prob, prob_outliers, type)
%#codegen

[m,n,~,q] = fcn_sizeMISO(A, B);
obj = Inf;
X = zeros(n,1);
w = zeros(m,1);
e = zeros(m,1);

max_i = fcn_subsamples (A, prob, prob_outliers); %number of random subsamples
h = floor(m/2)+floor((n+1)/2); %LTS quantile for highest break down [p.132, Rousseeuw1987]

%standardized the data-----------------------------------------------------
[dataStand, dataDenom, dataMedian] = ...
    fcn_robustStand([A, B], zeros(1,q),zeros(1,q),'standardize');
A = dataStand(:,1:n);
B = dataStand(:,q);
%--------------------------------------------------------------------------

i=1;
while i < max_i
    m_rand = randperm(m); %uniform random row numbers without replacement (unique)
    m_i = m_rand(1:n); %take the first n row numbers
    A_i = A(m_i,:);
    B_i = B(m_i,:);
    if isequal(A(:,end) , ones(m,1))==1 %input data with constant term in last column
      [~, matCheck] = fcn_myUnique([A_i(:,1:(n-1)), B_i]);
    else
      [~, matCheck] = fcn_myUnique([A_i, B_i]);
    end
    if matCheck==0 %no duplicate entries column-wise
        X_i = A_i \ B_i; %excact solution for this subsample
        e_i = B-A*X_i; %modell error
        %objective function of LMS or LTS----------------------------------
        switch type
            case 'LMS'        
            obj_i = median(e_i.^2); %median of sqaured model error
            case 'LTS'
            sortEquad_i = sort(e_i.^2); 
            obj_i = sum(sortEquad_i(1:h)); %sum of trimmed sqaured model error
        end
        %------------------------------------------------------------------  
        if obj_i < obj
           obj = obj_i;
           X=X_i;
           e=e_i;
        end
    i=i+1;
    else %there are duplicate entries colum-wise
    %try it one more time
    end
end

%retransform the standardized data-----------------------------------------
[dataRetrans, ~, ~] =  fcn_robustStand([A, B], dataDenom, dataMedian,'retransform');
A = dataRetrans(:,1:n);
B = dataRetrans(:,q);
X = X * dataDenom(q) ./dataDenom(1:n)';
obj = obj * dataDenom(q)^2;
e = e * dataDenom(q);
if isequal(A(:,end) , ones(m,1))==1 %input data with constant term in last column
   intercept = B - A(:,1:(n-1)) * X(1:(n-1)); %[p. 201, Rousseeuw1987]
   loc = fcn_LMSlocation(intercept);
   X(n) = loc; %replace the constant parameter
end
%--------------------------------------------------------------------------

%now postprocessing p.202--------------------------------------------------
s0 = 1.4826*( 1 + 5/(m - n) )*sqrt(obj); %primary estimated measurement stdev
    for j=1:m
        if abs(e(j)/s0) < 2.5
           w(j)=1; %no outlier
        else
           w(j)=0; %outlier
        end
    end
stdev = sqrt(1/(sum(w)-n)*(e'*diag(w)*e)); % estimated measurement stdev without outliers
e_stand = e/stdev;% scaled residual
P = stdev^2 * ( (A'*diag(w)*A)  \ eye(n)); % parameter covariance matrix
if isequal(A(:,n) , ones(m,1))==1 % input data with constant term in last column
    Rquad = 1-( median(abs(e)) / fcn_mad(B) )^2;
else  %input data without constant term in last column
    Rquad = 1-( median(abs(e)) / median(abs(B)) )^2;
end

end%=======================================================================


function i = fcn_subsamples (A, prob, prob_outliers)
%#codegen
%function to compute the number of required subsamples from the whole data
%set
%Inputs:
%A-input data size(m,n)
%prob-probability to pic at least one subsample without outliers 0.9...(1-eps)
%prob_outliers-expected fraction of outliers 0.050..0.5
% source: [p.198, Rousseeuw1987]

if prob >= 1
    error('wrong prob results in infinitive while loop')
end
%
if prob_outliers > 0.5 || prob_outliers < 0.05
    error('prob_outliers should be between 0.05 and 0.5')
end

[~ , n] = size(A);
%
i = ceil( log(1-prob) / log(1-(1-prob_outliers)^n) );
end

%=======================================================================
%standardize the data in a robust way
%source: [p.160, Rousseeuw1987]

function [dataOut, dataDenom, dataMedian] = ...
         fcn_robustStand(dataIn, dataDenom, dataMedian, type)
%#codegen

[m,q] = size(dataIn);
switch type
    case 'standardize'
    if isequal(dataIn(:,(q-1)) , ones(m,1))==1 %data with constant term in the secound last column
        dataDenom = 1.4826 * fcn_mad(dataIn);
        dataMedian = median(dataIn);
        dataOut = (dataIn - repmat(dataMedian ,m,1))./ ...
                  ( repmat( dataDenom , m,1) );
        dataOut(:,(q-1)) = ones(m,1); %replace the secound last column with constant term again
    else %input data without constant term in last column
        dataDenom = 1.4826 * median(abs(dataIn));
        dataMedian = median(dataIn);
        dataOut = dataIn ./ ...
                  ( repmat( dataDenom , m,1) );       
    end
    case 'retransform'
    if isequal(dataIn(:,(q-1)) , ones(m,1))==1 %data with constant term in the secound last column
       dataOut = dataIn * diag(dataDenom) + repmat(dataMedian,m,1);
    else
       dataOut = dataIn * diag(dataDenom);
    end
        
end

end%=======================================================================

function loc = fcn_LMSlocation(data)
%#codegen
%function for one dimensional LMS location estimate
%Input:
%data size (m,1)
%Output:
%loc-LMS location estimate
%source: [p. 169, Rousseeuw1987]


[m, ~] = size(data);
dataSort = sort(data);
h = floor(m/2)+1;
half=Inf; %initial half length
edges=zeros(1,2); %initalize edges
for j=1:m-h-1
   half_j = dataSort(j+h-1) - dataSort(j);
   if half_j < half %search for the shortest half
      edges =[dataSort(j) , dataSort(j+h-1)];
   end
end
loc = mean(edges); %LMS location estimate
end%=======================================================================

%function for the median absolute deviate which is a robust form of the
%standard deviation

function mad = fcn_mad(data)
%#codegen
m = size(data,1);    
mad = median(abs(data - repmat( median(data) , m,1 ) ) );
end

% function to ckeck if entries are column-wise unique in a matrix
% Input:
% data-data to check size(m,n)
% Output:
% colWise-is zero if data is column-wise unique or ~=zero size (1,n)
% matWise-is zero if all elements are unique

function [colWise, matWise] = fcn_myUnique(data)
%#codegen
sortData = sort(data); %sort data column-wise in ascending order
diffData = diff(sortData); %compute the differences
compToZero = (diffData == 0); %find unique elements
colWise = sum( compToZero ); %unique elements column-wise
matWise = sum( colWise ); %unique elements in matrix
end

%==========================================================================
% size check for MISO systems
% systems
% Inputs:
% A-system input size (m,n)
% B-system output size (m,d) with d:=1 for MISO systems
% Outputs:
% m-number of samples
% n-number of inputs (is equal with number of parameters)
% d-number of outputs
% q-n+d

function [m,n,d,q] = fcn_sizeMISO (A, B)
%#codegen

[m, n] = size(A);
[~, d] = size(B);
q=n+d;

end