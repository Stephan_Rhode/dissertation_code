%==========================================================================
% Solves the linear equation AX=B using robust M-Estimator
% for MISO systems
% Inputs:
% A-system input size (m,n)
% B-system output size (m,d) with d:=1 for MISO systems
% type-type of robust function 'Huber'
% Outputs:
% X-the system parameters size (n,d)

function [X, P, squad] = fcn_M_Estimator(A, B, type)
%#codegen
[m,n,~,~] = fcn_sizeMISO(A, B);
W = ones(m,1);
[X, P, e, ~, squad]  = fcn_gls(A,B,W);
X_ = zeros(n,1); % approximation on the previous iteration
tol = 1e-10; % in percent
%
while 100*abs((X - X_) ./X_) > tol(ones(n,1))
    X_ = X;
    stdevHat = 1.483 * fcn_mad(e);
    for i = 1 : m
        W(i,1) = fcn_mCostFunction (e(i) / stdevHat, type);    
    end
    [X, P, e, ~, squad]  = fcn_gls(A,B,W);
end
end % of function
%
%==========================================================================
function [X, P, e, obj, squad]  = fcn_gls(A,B,G)
%#codegen
% Solves the linear equation AX=B using weighted least-squares
% Input:
% A-system input size(m,n)
% B-system output size(m,1)
% G-the weights size(m,1) for weighted least squares
% (var(B)=sigma^2*diag(G), where var(B) is not constant and uncorrelated)
% or 
% G-the error covariance size(m,m) for generalized least squares
% (var(B)=sigma^2*G, where var(B) is not constant and correlated)
% Output:
% X-the system parameters size(n,1)
% P-parameter covariance size(n,n)
% squad-measurenment variance

[m,n,~,~] = fcn_sizeMISO (A, B);

if isvector(G)==1 %weighted least squares
    W=diag(G);
elseif size(G,1)==m && size(G,2)==m %generalized least sqaures
    W=G\eye(m);
else
    error('wrong size of G')
end
X = (A'*W*A)\(A'*W*B); %modell parameter
e = B - A * X; %modell error
obj = (e'*W*e); %sum of squared residuals
squad = 1/(m-n)*obj; %estimated measurement variance
P = squad * ((A'*W*A)\eye(n));
end
%==========================================================================

%function for the median absolute deviate which is a robust form of the
%standard deviation

function mad = fcn_mad(data)
%#codegen
m = size(data,1);    
mad = median(abs(data - repmat( median(data) , m,1 ) ) );
end

%==========================================================================
% size check for MISO systems
% systems
% Inputs:
% A-system input size (m,n)
% B-system output size (m,d) with d:=1 for MISO systems
% Outputs:
% m-number of samples
% n-number of inputs (is equal with number of parameters)
% d-number of outputs
% q-n+d

function [m,n,d,q] = fcn_sizeMISO (A, B)
%#codegen

[m, n] = size(A);
[~, d] = size(B);
q=n+d;

end
