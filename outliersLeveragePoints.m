function [] = outliersLeveragePoints(type)
%% AX=B create the system
m = 7;
Atrue = [(1:m)', ones(m,1)];
Xtrue = [3;0];
Btrue = Atrue*Xtrue;
lim = Btrue(end)+1;
% noise
noiseVar = 0.1;
rng('default');
Bnoise = 0 + sqrt(noiseVar)*randn(m,1);
B = Btrue + Bnoise;
A = Atrue;
% outlier in B
Bout = B;
Bout(2) = 20;
% outlier in A
Aout = A;
%##############--> play here
    Aout(4,1) = 13.75;
%##############--> play here
% "good" leverage point
Alev = A;
Alev(m,1) = 15;
levlim = Alev(m,:) * Xtrue + 1; 
rng('default');
Blev = Alev * Xtrue + sqrt(noiseVar)*randn(m,1);

switch(type)
    case 'LS' % LS only
        XhatLS1 = A\B;
        BhatLS1 = A*XhatLS1;
        %
        XhatLS2 = A\Bout;
        BhatLS2 = A*XhatLS2;
        %
        XhatLS3 = Aout\B;
        BhatLS3 = Aout*XhatLS3;
        %
        XhatLS4 = Alev\Blev;
        BhatLS4 = Alev*XhatLS4;
        % plots
        figure
        subplot(2,2,1)
        hold on
        scatter(A(:,1),B)
        plot(A(:,1), BhatLS1, 'k')
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        subplot(2,2,2)
        hold on
        scatter(A(:,1),Bout)
        plot(A(:,1), BhatLS2, 'k')
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        plot(A(2,1),B(2),'*')
        subplot(2,2,3)
        hold on
        scatter(Aout(:,1),B)
        plot(Aout(:,1), BhatLS3, 'k')
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        plot(A(4,1),B(4),'*')        
        subplot(2,2,4)
        hold on
        scatter(Alev(:,1),Blev)
        plot(Alev(:,1), BhatLS4, 'k')
        xlabel('A'), ylabel('B')
        xlim([0 levlim])
        ylim([0 levlim])
        legend('data','LS')
        % save to txt
        file = 'outliersLeveragePoints';
        fid = fopen([file '.txt'],'w');
        header = ['A\t' 'B\t' 'Aout\t' 'Bout\t' 'Alev\t' 'Blev\t'...
                  'BhatLS1\t'  'BhatLS2\t' 'BhatLS3\t' 'BhatLS4\n'];  
        data = [A(:,1), B, Aout(:,1), Bout, Alev(:,1), Blev,...
                BhatLS1, BhatLS2, BhatLS3, BhatLS4];
        fprintf(fid, header);
        str = '%g\t';
        fprintf(fid,[repmat(str,1,size(data,2)-1) '%g\n'],data');
        fclose(fid);
    case 'robust' % LS and robust estimators
        %% LS
        XhatLS1 = A\B;
        BhatLS1 = A*XhatLS1;
        %
        XhatLS2 = A\Bout;
        BhatLS2 = A*XhatLS2;
        %
        XhatLS3 = Aout\B;
        BhatLS3 = Aout*XhatLS3;
        %
        XhatLS4 = Alev\Blev;
        BhatLS4 = Alev*XhatLS4;
        %% LMS
        prob = 1-eps;
        prob_outliers = 0.5; 
        %
        XhatLMS1  = fcn_LMS_LTS(A, B, prob, prob_outliers, 'LMS');
        BhatLMS1 = A*XhatLMS1;
        %
        XhatLMS2  = fcn_LMS_LTS(A, Bout, prob, prob_outliers, 'LMS');
        BhatLMS2 = A*XhatLMS2;
        %
        XhatLMS3  = fcn_LMS_LTS(Aout, B, prob, prob_outliers, 'LMS');
        BhatLMS3 = Aout*XhatLMS3;
        %
        XhatLMS4  = fcn_LMS_LTS(Alev, Blev, prob, prob_outliers, 'LMS');
        BhatLMS4 = Alev*XhatLMS4;
        %% M-Estimator       
        %
        XhatMHuber1 = fcn_M_Estimator(A, B, 'Huber');
        BhatMHuber1 = A * XhatMHuber1;
        XhatMMyriad1 = fcn_M_Estimator(A, B, 'Myriad');
        BhatMMyriad1 = A * XhatMMyriad1;
        %
        XhatMHuber2 = fcn_M_Estimator(A, Bout, 'Huber');
        BhatMHuber2 = A * XhatMHuber2;
        XhatMMyriad2 = fcn_M_Estimator(A, Bout, 'Myriad');
        BhatMMyriad2 = A * XhatMMyriad2;        
        %
        XhatMHuber3 = fcn_M_Estimator(Aout, B, 'Huber');
        BhatMHuber3 = Aout * XhatMHuber3;
        XhatMMyriad3 = fcn_M_Estimator(Aout, B, 'Myriad');
        BhatMMyriad3 = Aout * XhatMMyriad3;        
        %
        XhatMHuber4 = fcn_M_Estimator(Alev, Blev, 'Huber');
        BhatMHuber4 = Alev * XhatMHuber4;
        XhatMMyriad4 = fcn_M_Estimator(Alev, Blev, 'Myriad');
        BhatMMyriad4 = Alev * XhatMMyriad4;        
        %
        % plots
        figure
        subplot(2,2,1)
        hold on
        scatter(A(:,1),B)
        plot(A(:,1), BhatLMS1, 'k')
        plot(A(:,1), BhatMHuber1, 'r--')
        plot(A(:,1), BhatMMyriad1, 'b:')        
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        subplot(2,2,2)
        hold on
        scatter(A(:,1),Bout)
        plot(A(:,1), BhatLMS2, 'k')
        plot(A(:,1), BhatMHuber2, 'r--')
        plot(A(:,1), BhatMMyriad2, 'b:')        
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        plot(A(2,1),B(2),'*')
        subplot(2,2,3)
        hold on
        scatter(Aout(:,1),B)
        plot(Aout(:,1), BhatLMS3, 'k')
        plot(Aout(:,1), BhatMHuber3, 'r--')
        plot(Aout(:,1), BhatMMyriad3, 'b:')        
        xlabel('A'), ylabel('B')
        xlim([0 lim])
        ylim([0 lim])
        plot(A(4,1),B(4),'*')  
        subplot(2,2,4)
        hold on
        scatter(Alev(:,1),Blev)
        plot(Alev(:,1), BhatLMS4, 'k')
        plot(Alev(:,1), BhatMHuber4, 'r--')
        plot(Alev(:,1), BhatMMyriad4, 'b:')  
        xlabel('A'), ylabel('B')        
        xlim([0 levlim])
        ylim([0 levlim])
        legend('data','LMS','M-Huber','M-Myriad')
        % save to txt
        file = 'outliersLeveragePointsRobust';
        fid = fopen([file '.txt'],'w');
        header = ['A\t' 'B\t' 'Aout\t' 'Bout\t' 'Alev\t' 'Blev\t'...
                  'BhatLS1\t'  'BhatLS2\t' 'BhatLS3\t' 'BhatLS4\t'...
                  'BhatLMS1\t'  'BhatLMS2\t' 'BhatLMS3\t' 'BhatLMS4\t'...
                  'BhatMHuber1\t'  'BhatMHuber2\t' 'BhatMHuber3\t' 'BhatMHuber4\t'...
                  'BhatMMyriad1\t'  'BhatMMyriad2\t' 'BhatMMyriad3\t' 'BhatMMyriad4\n'];  
        data = [A(:,1), B, Aout(:,1), Bout, Alev(:,1), Blev,...
               BhatLS1, BhatLS2, BhatLS3, BhatLS4,...
               BhatLMS1, BhatLMS2, BhatLMS3, BhatLMS4,...
               BhatMHuber1, BhatMHuber2, BhatMHuber3, BhatMHuber4,...
               BhatMMyriad1, BhatMMyriad2, BhatMMyriad3, BhatMMyriad4];
        fprintf(fid, header);
        str = '%g\t';
        fprintf(fid,[repmat(str,1,size(data,2)-1) '%g\n'],data');
        fclose(fid);
    otherwise
        error('wrong input string')
end
end % of function