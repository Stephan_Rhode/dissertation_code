% Kalman Filter============================================================
%
% Inputs:
% x_old - old state vector, size (n,1)
% P_old - old covariance matrix, size (n,n)
% A - input data, size (1,n)
% B - desired signal or output, size (1)
% Aiv - instument, size (1,n)
% calA...calD - state transition matrices, see type.StatisticModel
% robScale_old - [stdev of a posteriori error;...
%            median of squared posteriori errors];
% type - a struct that controls the used FilterType, CostFCN...
%
% Outputs:
% X_new - new state vector
% P_new - new covariance matrix
% robScale_new - [stdev of a posteriori error;...
%            median of squared posteriori errors];  
% W-weighting
%
function [X_new, P_new, robScale_new, W]  = ...
    fcn_LKFParaEst(X_old, P_old, A, B, Aiv, robScale_old, type)
%#codegen
robScale_new = robScale_old; %initialisation
n = numel(X_old);
    %
deltaB = B - A * X_old; %a priori error
W = fcn_mCostFunction (deltaB / robScale_old(1), type.CostFcn);    
R = type.R;  
    if strcmp(type.FilterType , 'RLS')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            %RLS KF connection
                    % Q = P_old * (1/R - 1) + (K * A * P_old)*(1 - 1/R); %RLS update
                    % P_new = P_old - K * A * P_old + Q; %RLS update
            P_new = 1/R*(P_old - K * A * P_old); %[Ljung1999a, p.365]
            X_new = X_old + K * deltaB;
    elseif strcmp(type.FilterType , 'LevenbergMarquardtRLS')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');
            P_new = 1/R*(P_old - K * A * P_old);      
            mu = type.mu;
            P_new = P_new / (eye(n) + mu * P_new); %[Gunnarsson1996]            
            X_new = X_old + K * deltaB;
    elseif strcmp(type.FilterType , 'TikhonovRLS')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            P_new = 1/R*(P_old - K * A * P_old);       
            mu = type.mu;
            P_new = P_new / (eye(n) + mu * P_new); %[Gunnarsson1996]           
            X_new = X_old + K * deltaB - P_new * mu * X_old; %[VanWaterschoot2008]         
    elseif strcmp(type.FilterType , 'KF')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            Q = type.Q;
            P_new = P_old - K * A * P_old + Q; %[Stenlund2002]
            X_new = X_old + K * deltaB;
    elseif strcmp(type.FilterType , 'SRKF') %Square Root measurement update via triangularization
            %Source: Simon 2006; Optimal state estimation; Chapter 6 Seite 169
            %And: Kaminski 1971; Discrete square root filtering: A survey of current techniques; Seite 731         
            S_oldApos = P_old; %lower triangular we use the buffer of P
            Q = type.Q;
            Qsr = chol(Q,'lower');
            Rsr = chol(R,'lower');
            prearray = [Rsr            , zeros(1,n);...
                       (S_oldApos'*A') , S_oldApos'];     
            postarray = fcn_house(prearray); %or just %[~,postarray]= qr(prearray);
            Kk = postarray(1,2:(n+1))';
            r = postarray(1,1);
            S_new = postarray(2:(n+1),2:(n+1))';
            K = (Kk/r);
            X_new = X_old + K * deltaB;              
            S_new = fcn_modGramSmith([S_new';Qsr']);            
            P_new = S_new';
    elseif strcmp(type.FilterType , 'LevenbergMarquardtKF')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            Q = type.Q;
            P_new = P_old - K * A * P_old + Q;
            mu = type.mu;
            P_new = P_new / (eye(n) + mu * P_new); %[Gunnarsson1996]            
            X_new = X_old + K * deltaB;
    elseif strcmp(type.FilterType , 'TikhonovKF')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            Q = type.Q;
            P_new = P_old - K * A * P_old + Q;
            mu = type.mu;
            P_new = P_new / (eye(n) + mu * P_new); %[Gunnarsson1996] 
            X_new = X_old + K * deltaB - P_new * mu * X_old; %[VanWaterschoot2008]                      
    elseif strcmp(type.FilterType , 'SGKF')
            K = (W * P_old * Aiv') / (R + W * A * P_old * Aiv');         
            Qd = type.Qd;
            Q = (W * Qd * Aiv' * A * Qd)/(R + W * A * Qd * Aiv');
            P_new = P_old - K * A * P_old + Q; %[Stenlund2002]
            X_new = X_old + K * deltaB;
    end
%
if strcmp(type.robScale, 'On')
    deltaB =  B - A * X_new; %a posteriori error
    p = 0.5; %for median
    eta = 0.01; %learn-rate
    data = deltaB^2; %learn squarred output corrections
    stdh_old =  robScale_old(1);
    quantile_old = robScale_old(2);
    quantile_new = quantile_old + eta * abs(sign(data - quantile_old)) *...
        (sign(data - quantile_old) + 2.0 * p - 1.0); %median error quad learning
    robScale_new(2) = quantile_new;
    lambda_stdh = 0.995;
    stdh_new = real(sqrt(lambda_stdh * stdh_old^2 + (1 - lambda_stdh) * 1.483 * quantile_new));
    robScale_new(1) = stdh_new;
elseif strcmp(type.robScale, 'Off')
    %
else
    error('type.robScale not defined')
end
end % of function==========================================================
%
function W = fcn_modGramSmith(A)
%source: [Simon2006, p.172]
[~,n] = size(A);
W = zeros(n);
for k=1:n %Zeile
    sigma = sqrt(A(:,k)'*A(:,k));
    for j=1:n %Spalte
        if j==k
            W(k,j)=sigma;
        elseif j==k+1 || j==n
            W(k,j)=(A(:,k)'*A(:,j))/sigma;
        else
            W(k,j)=0;
        end
    end
end

end % of function==========================================================
%
function A = fcn_house(A)
%source: [Golub1996, p.211]
[m,n] = size (A);
for j=1:(n-1)
[v, beta] = fcn_householderVector(A(j:m,j));
A(j:m , j:n) = (eye(m-j+1) - beta*(v*v')) * A(j:m , j:n);
end
end % of function==========================================================
%
function [v, beta] = fcn_householderVector(x)
%source: [Golub1996, p.210]
n = length(x);
sigma = x(2:n)'*x(2:n);
v=[1;...
    x(2:n)];
if sigma == 0
    beta =0;
else
    mu = sqrt(x(1)^2+sigma);
    if x(1) <= 0
        v(1)= x(1) - mu;
    else
        v(1) = -sigma / (x(1) + mu);
    end
    beta = 2*v(1)^2 / (sigma + v(1)^2);
    v = v / v(1);
end
end % of function