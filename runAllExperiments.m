% this file runs all experiments within the dissertation
% Experiment 3.1
outliersLeveragePoints('LS');
% Experiment 3.2
outliersLeveragePoints('robust');
% Experiment 3.3
linearMISOoe('RLS');
linearMISOoe('RLM');
linearMISOoe('Levenberg-Marquardt-RRLM');
linearMISOoe('Tikhonov-RRLM');
% Experiment 3.4
linearMISOeiv('Levenberg-Marquardt-RRLM');
linearMISOeiv('Levenberg-Marquardt-RRIVM');
% Experiment 3.5
% you have to download
% Experiment 3.6
linearRWeiv('Levenberg-Marquardt-RRIVM');
linearRWeiv('Levenberg-Marquardt-RIVMKF');
% Experiment 3.7
linearPFoe();