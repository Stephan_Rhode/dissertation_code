function linearPFoe()
%
m=1e4; %samples
inputTuning = [6e-3 ,12e-3 ,14e-3];
X_true =  [1 1.5 5000; ...Parameter 1 auf 2 bei sample 5000
           2 2      1; ...
           3 3      1 ];
NoiseSigma2 = [0 0 0 0.1];...RLS case
windup =  ...Jede Spalte ist f�r einen Eingangskanal
          ...Oberer Wert Windup Anfang unterer Windup Ende
          ...Deaktivieren je Eingangskanal mit [1;1]
          ...  [1 1 1;...
          ...   1 1 1];...
         [1 1000 1;...
          1 6000 1];...
n = size(inputTuning,2);
t = repmat((1:m)', 1, n);
Arg = repmat(inputTuning,m,1).* t;
A_true = sin(2*pi*Arg).*sin(2*pi*Arg/3.3); % modulierter sinus A
for i=1:n
    lengthWindup = windup(2,i) - windup(1,i);
    shiftSignal = A_true(windup(1,i):end,i);
    A_true(windup(1,i):windup(2,i)-1,i) = repmat(A_true(windup(1,i),i), lengthWindup, 1);
    A_true(windup(2,i):end, i) = shiftSignal(1:end-lengthWindup,:);
end
%
if size(X_true,2)==3
    B_true=zeros(m,1);
    xshiftTrue = zeros(n,m);
    for i=1:n
       xshiftTrue(i,:) = [repmat(X_true(i,1), 1, X_true(i,3)-1),... %Parameter Anfang
                          repmat(X_true(i,2), 1, m-X_true(i,3)+1)];  %Parameter Sprung bis Ende
    end
    X_true = xshiftTrue;
    %    
    for i=1:m
        B_true(i) = A_true(i,:) * X_true(:,i);
    end
else
    error('size X_true falsch')   
end
NoiseSigma2 = repmat(NoiseSigma2,m,1);
rng('default');
Noise = sqrt(NoiseSigma2).* randn(m,n+1); %add white Gaussian noise to all inputs
A_mess = A_true + Noise(:,1:n);
B_mess = B_true + Noise(:,n+1);
%% PKS setup
Order = 4;
shiftIncrement = 1;
leftWindow = 20;
rightWindow = leftWindow;
windowSize = leftWindow + rightWindow + 1;
[calA, u, uSmooth] = fcn_PKSFilterInit(Order, shiftIncrement, ...
    leftWindow, rightWindow);
PKS_X = zeros(Order+1,1);
PKS_P = 100*eye(Order+1, Order+1);
lambda = 0.9;
smoothSignal = zeros(m,1);
MSE_PKS = zeros(m,1);
FIR = zeros(m,1);
MSE_FIR = zeros(m,1);
for i = 1 + windowSize : m - windowSize
    [PKS_X, PKS_P] = fcn_PKS(PKS_X, PKS_P, u, B_mess(i,:), calA, lambda);
    smoothSignal(i - rightWindow) = uSmooth * PKS_X;
    MSE_PKS(i - rightWindow) = 1/i*norm(B_true(i - rightWindow) - smoothSignal(i - rightWindow))^2;
end
% FIR = filter(ones(1,windowSize)/windowSize,1,B_mess);
for i = 1 + windowSize : m - windowSize
    FIR(i - rightWindow) = mean(B_mess(i - windowSize : i));
    MSE_FIR(i - rightWindow) = 1/i*norm(B_true(i - rightWindow) - FIR(i - rightWindow))^2;
end
%% plots
figure
lnk(1) = subplot(1,2,1);
plot((1:m), B_mess, '.k', (1:m), smoothSignal, '.r',...
    (1:m), FIR,'.b')
ylim([0.9 * min(B_true) , 1.1 * max(B_true)]);
xlim([600,900])
legend('B_{mess}','Bhat_{PKS}','Bhat_{FIR}')
xlabel('time');
ylabel('B_{hat}');
lnk(2) = subplot(1,2,2);
plot((1:m) , MSE_PKS, '.r', (1:m) , MSE_FIR, '.b')
xlim([600,900])
legend('PKS','FIR')
xlabel('time');
ylabel('MSE');
linkaxes([lnk(1),lnk(2)],'x');
title('linearPFoe')
% write txt file
fid = fopen('linearPFoe.txt', 'w');
header = ['B_true\t' 'B_mess\t' 'PKS\t' 'FIR\t' 'MSE_PKS\t' 'MSE_FIR\t'...
          'time\n'];  
result = [B_true, B_mess, smoothSignal, FIR,...
       MSE_PKS, MSE_FIR, (1:m)']; 
fprintf(fid, header);
str = '%g\t';
fprintf(fid,[repmat(str,1,size(result,2)-1) '%g\n'],result');
fclose(fid);
end % of function

%==========================================================================
function [calA, u, uSmooth] = fcn_PKSFilterInit(Order, shiftIncrement, ...
    leftWindow, rightWindow)
% function for setting up the PKS Filter
%
% Inputs:
% Order - Number of polynominal factors a_n  (a_0 + a_1*s + a_2*s^2 + ..)
% shiftIncrement - step size for state transition (commonly 1 in discrete systems)
% leftWindow - number of points left hand site of smoothing point
% rightWindow - number of points right hand site of smoothing point
% lambda - factor for exponential forgetting (lambda = 1-1/FilterLength)
% Outputs:
% calA - state transition matrix in the statistical model:
%
% xApos(t)     = calA x(t-1) (for random walk calA = I)
% x(t)         = xApos(t)  + v
% d(t)         = calC x(t) + w
%
% with calC    = u(t)
% with Cov(v)  = R
% with Cov(w)  = Q
%
% K - Kalman gain, time invariant, size (n,1)
% u - input data, time invariant, size (1,n)
% uSmooth - input data for smooth filter output
% compute the filter output with:
% smoothSignal = uSmooth * X;
% firstDerivateSignal = (0:1:length(X)-1) * ([0,uSmooth(1:end-1)]'.*X);
%
%
FilterLength = leftWindow + rightWindow + 1;
Koefficients = Order + 1;
u = zeros(1 , Koefficients); %init values
uSmooth = zeros(1 , Koefficients); %init values
for i=1:Koefficients
    u(i) = FilterLength^(i-1);
    uSmooth(i) = (leftWindow +1)^(i-1);
end   
calA = fcn_calA(Koefficients, shiftIncrement); %compute state transition matrix
end % of function
%==========================================================================


%==========================================================================
% function to compute the state transition matrix calA in the statistical
% model
% statistical model:
% xApos(t)     = calA x(t-1) (for random walk calA = I)
% x(t)         = xApos(t)  + v
% d(t)         = calC x(t) + w
%
% with calC    = u(t)
% with Cov(v)  = R
% with Cov(w)  = Q
%
% Inputs:
% N : Number of polynominal factors a_n  (a_0 + a_1*s + a_2*s^2 + ..)
% ds : Shift length
%
% Outputs:
% calA : Shift matrix for a polynominal function 
% @Felix Bleimund 2013 KIT
%
function calA = fcn_calA(N, ds)
calA = zeros(N);
calA(1, :) = ones(1,N);
% Derived in hand written notes from 2013-05-16 
% "2013-05-19_notes to polynominal shif matrix.pdf"
for i = 2:N
    for j = 2:N
        calA(i,j) = (calA(i-1,j-1) +  calA(i,j-1)) ;
    end
end
%
for i = 1:N
    for j=1:N
        calA(i,j) = calA(i,j)*ds^(j-i);
    end
end

end % of function==========================================================
%==========================================================================

%==========================================================================
function [x_new, P_new]  = ...
    fcn_PKS(x_old, P_old, u, d, calA, R)
% Inputs:
% x_old - old state vector, size (n,1)
% P_old - old covariance matrix, size (n,n)
% u - input data, size (1,n)
% d - desired signal or output, size (1)
% calA...calD - state transition matrices, see type.StatisticModel
%
% Outputs:
% x_new - new state vector
% P_new - new covariance matrix 
%
%
%==========================================================
% statistical model:
% xApos(t)     = calA x(t-1) (for random walk calA = I)
% x(t)         = xApos(t)  + v
% d(t)         = calC x(t) + w
%
% with calC    = u(t)
% with Cov(v)  = R
% with Cov(w)  = Q
%==========================================================  
%propagation
% calB = 0;
calC = u;
% calD = 0;
%time update
x_new = calA * x_old;
P_new = calA * P_old * calA';
%
e = d - (calC * x_new);
K = (P_new * calC') / (R + calC * P_new * calC');
P_new = 1/R*(P_new - K * calC * P_new); %[Ljung1999a, p.365]
x_new = x_new + K * e;
end % of function
%==========================================================================