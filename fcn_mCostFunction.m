%==========================================================================
function W = fcn_mCostFunction (e, type)
%
if e == 0 %to prevent division by zero -> NaN
   e = eps;
end

%
switch type
    case 'L1' %do not use L1! it is not convex and hence not stable
        psi = sign(e);
    case 'L2' %known as ordinary least squares
        psi = e;
    case 'L1-L2'
        psi = e/(sqrt(1+e^2/2));
    case 'Huber'   
        a = 1.345; %95% asymptotic efficiency
        if  abs(e) < a
            psi = e;
        else
            psi = a * sign(e);
        end
    case 'Cauchy'
        c = 2.3849;  %95% asymptotic efficiency
        psi = e/(1+(e/c)^2);
    case 'alphaDetector' %[Seyfe2005] exponentional decreasing function
        alpha = 0.00001;
        psi = e * exp(-alpha * e^2);
    % from now redescending M-estimators
    case 'Hampel' %three-part redescending M-estimator [p.148, Rousseeuw1987]
        a = 1.96; %95% normal distribution
        b = 2.24; %97.5% normal distribution
        c = 2.576; %99% nomal distribution
        if  abs(e) < a
            psi = e;
        elseif a <= abs(e) && abs(e) < b
            psi = a * sign(e);
        elseif b <= abs(e) && abs(e) <= c
            psi = ((c-abs(e))/(c-b)) * a * sign(e);
        else
            psi = 0;
        end 
    case 'Tukey' %redescending M-estimator
        c = 4.6851; %95% asymptotic efficiency
        if  abs(e) < c
            psi = e*(1-e^2/c^2)^2;
        else
            psi = 0;
        end
    case 'Myriad' %[p.105, Brabanter2009]
        beta = 1;
        psi = (beta^2 * e)/(beta^2 + e^2);
    otherwise
    error('wrong type')
end
    W = psi/e; %weighting function
end % of function
%==========================================================================