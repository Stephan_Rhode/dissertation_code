function [] = linearMISOoe(cases)
m=1e4; %samples
inputTuning = [6e-3 ,12e-3 ,14e-3];
X_true =  [1 1.5 5000; ...Parameter 1 auf 2 bei sample 5000
           2 2      1; ...
           3 3      1 ];
NoiseSigma2 = [0 0 0 0.1];...RLS case
windup =  ...Jede Spalte ist f�r einen Eingangskanal
          ...Oberer Wert Windup Anfang unterer Windup Ende
          ...Deaktivieren je Eingangskanal mit [1;1]
          ...  [1 1 1;...
          ...   1 1 1];...
         [1 1000 1;...
          1 6000 1];...    

n = size(inputTuning,2);
t = repmat((1:m)', 1, n);
Arg = repmat(inputTuning,m,1).* t;
A_true = sin(2*pi*Arg).*sin(2*pi*Arg/3.3); % modulierter sinus A
for i=1:n
    lengthWindup = windup(2,i) - windup(1,i);
    shiftSignal = A_true(windup(1,i):end,i);
    A_true(windup(1,i):windup(2,i)-1,i) = repmat(A_true(windup(1,i),i), lengthWindup, 1);
    A_true(windup(2,i):end, i) = shiftSignal(1:end-lengthWindup,:);
end
%
if size(X_true,2)==3
    B_true=zeros(m,1);
    xshiftTrue = zeros(n,m);
    for i=1:n
       xshiftTrue(i,:) = [repmat(X_true(i,1), 1, X_true(i,3)-1),... %Parameter Anfang
                          repmat(X_true(i,2), 1, m-X_true(i,3)+1)];  %Parameter Sprung bis Ende
    end
    X_true = xshiftTrue;
    %    
    for i=1:m
        B_true(i) = A_true(i,:) * X_true(:,i);
    end
else
    error('size X_true falsch')   
end
NoiseSigma2 = repmat(NoiseSigma2,m,1);
rng('default');
Noise = sqrt(NoiseSigma2).* randn(m,n+1); %add white Gaussian noise to all inputs
A_mess = A_true + Noise(:,1:n);
B_mess = B_true + Noise(:,n+1);
% setup with outliers
Outlier=ones(m,n+1); %augumented data
outlierFraction = 0.05;
row = randperm(m, floor(m*outlierFraction));
col = randi(n+1, 1, floor(m*outlierFraction));
idx = sub2ind(size(Outlier), row, col); %linear indexing
%
for k=1:numel(idx)
    Outlier(idx(k)) = unifrnd(-4,4,1,1);
end
%
A_mess = A_mess .* Outlier(:,1:n);
B_mess = B_mess .* Outlier(:,n+1);

%% run estimator
%
data = zeros(m,n+4); %preallocation
%
switch(cases)
    case 'RLS'
        type.FilterType = 'RLS';
        type.CostFcn = 'L2';
        type.robScale = 'Off';
    case 'RLM'
        type.FilterType = 'RLS';
        type.CostFcn = 'Huber';  
        type.robScale = 'On';
    case 'Levenberg-Marquardt-RRLM'
        type.FilterType = 'LevenbergMarquardtRLS';
        type.mu = 0.1;
        type.CostFcn = 'Huber';
        type.robScale = 'On';
   case 'Tikhonov-RRLM'
        type.FilterType = 'TikhonovRLS';
        type.mu = 0.001;
        type.CostFcn = 'Huber';
        type.robScale = 'On';
   otherwise
        error('wrong input string')
end
type.R = 0.995;
X_hat=zeros(n,1); %Initial Parameter RLS
P_hat=100*eye(n); %init RLS
robScale_hat = [0.1;.01];
for i = 1:m
    A = A_mess(i,:);
    B = B_mess(i,:);
    Aiv = A; %no instruments
        [X_hat, P_hat, robScale_hat, W]  = fcn_LKFParaEst...
            (X_hat, P_hat, A, B, Aiv, robScale_hat, type);
 SEVN = norm(X_true(:,i) - X_hat)^2;
 data(i,:)=[X_hat', robScale_hat(1), W, SEVN, i]; %nur zum zwischenspeichern    
end
figure
subplot(2,3,1)
plot(data(:,end),data(:,1),'k.',data(:,end),data(:,2),'r.',data(:,end),data(:,3),'b.');
hold on
plot(data(:,end), X_true(1,:), 'k', data(:,end), X_true(2,:), 'r', data(:,end), X_true(3,:), 'b')
ylim([-.25 4.25]);
legend('Xhat_1', 'Xhat_2', 'Xhat_3', 'Xtrue_1', 'Xtrue_2', 'Xtrue_3');
xlabel('time');
ylabel('Xhat');
title(['linearMISOoe-' cases])
lnk(1)=subplot(2,3,2);
hold on
plot(A_mess(:,1),'r');
plot(A_mess(:,2),'k');
plot(A_mess(:,3),'b');
plot(B_mess(:,1),'g');
legend('A_1','A_2','A_3','B');
xlabel('time');
ylabel('A, B');
title(['linearMISOoe-' cases])
subplot(2,3,3)
plot(data(:,end),data(:,4),'k.',data(:,end), sqrt(NoiseSigma2(:,n+1)),'k');
legend('stdh','std');
xlabel('time');
ylabel('stdh');
title(['linearMISOoe-' cases])
subplot(2,3,4)
plot(data(:,end), data(:,6),'k')
xlabel('time');
ylabel('SEVN');
title(['linearMISOoe-' cases])
lnk(2)=subplot(2,3,5);
plot(data(:,5),'k');
xlabel('time');
ylabel('weight');
linkaxes([lnk(1),lnk(2)],'x');
title(['linearMISOoe-' cases])
% write txt file
fid = fopen(['linearMISOoe_' cases '.txt'],'w');
header = ['A1\t' 'A2\t' 'A3\t' 'B\t' 'X_hat1\t' 'X_hat2\t' 'X_hat3\t'...
          'stdev\t' 'W\t' 'SEVN\t' 'time\n'];  
result = [A_mess(:,1), A_mess(:,2), A_mess(:,3), B_mess(:,1),...
       data(:,1), data(:,2), data(:,3), data(:,4), data(:,5), data(:,6), data(:,end)]; 
fprintf(fid, header);
str = '%g\t';
fprintf(fid,[repmat(str,1,size(result,2)-1) '%g\n'],result');
fclose(fid);        
end % of function